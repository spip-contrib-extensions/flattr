<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// C
	'cfg_explication_uid' => 'You can find this information on <a href="https://flattr.com/dashboard" target="flattr">your Flattr account page</a>.',
	'cfg_label_category' => 'Default category',
	'cfg_label_showthings' => 'Show &#8216;things&#8217; on flattr.com',
	'cfg_label_uid' => 'User ID',
	'cfg_legend_reglages' => 'Misc settings',
	'cfg_legend_vous' => 'You',
	'cfg_value_audio' => 'Audio',
	'cfg_value_hide' => 'Hide',
	'cfg_value_images' => 'Images',
	'cfg_value_rest' => 'Rest',
	'cfg_value_show' => 'Yes, show',
	'cfg_value_software' => 'Sofware',
	'cfg_value_text' => 'Text',
	'cfg_value_video' => 'Video',
);
?>

<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// C
	'cfg_explication_uid' => 'Vous trouverez cette information sur <a href="https://flattr.com/dashboard" target="flattr">la page de votre compte Flattr</a>.',
	'cfg_label_category' => 'Cat&eacute;gorie par d&eacute;faut',
	'cfg_label_showthings' => 'Afficher les &#8216;things&#8217; sur flattr.com',
	'cfg_label_uid' => 'Nom d\'utilisateur',
	'cfg_legend_reglages' => 'R&#233;glages divers',
	'cfg_legend_vous' => 'Vous',
	'cfg_value_audio' => 'Audio',
	'cfg_value_hide' => 'Masquer',
	'cfg_value_images' => 'Images',
	'cfg_value_rest' => 'Divers',
	'cfg_value_show' => 'Oui, afficher',
	'cfg_value_software' => 'Logiciel',
	'cfg_value_text' => 'Texte',
	'cfg_value_video' => 'Vid&eacute;o',
	
);
?>
